'use strict';

const PREFIX = lochaCrypto.bitcoinjs.networks.testnet;
const turpial = {
    serviceUUID: "6e400001-b5a3-f393-e0a9-e50e24dcca9e",
    txCharacteristic: "6e400002-b5a3-f393-e0a9-e50e24dcca9e",
    rxCharacteristic: "6e400003-b5a3-f393-e0a9-e50e24dcca9e"
};

const broadcastContact = {
    id: "broadcast",
    alias: "broadcast",
    chat: [],
    draft_input: ""
};

const demoContacts = [
    {
        id: "broadcast",
        alias: "broadcast",
        chat: [],
        draft_input: ""
    },
    {
        id: "c51e1ca18038e6bb369cc5578691c9a92c41d892",
        alias: "ReplayBee",
        chat: [],
        draft_input: ""
    },
    {
        id: "b31e1ca18038e6bb369cc5578691c9a92c41d801",
        alias: "RomDry",
        chat: [],
        draft_input: ""
    },
    {
        id: "afb8beabe9a1859f61d35c036c1b4a8ff1df8b5b",
        alias: "JazzBeer",
        chat: [
            { type: 'sent', txt: 'hello JazzBeer!', time: 150000000100, hash160: '' },
            { type: 'received', txt: 'Hi Bro! How are u?', time: 150000000200, hash160: '' }
        ],
        draft_input: ""
    }
];
const transactions = {

};

const storageSet = function (k, v) {
    window.localStorage.setItem(k, v);
};

const storageGet = function (k) {
    return JSON.parse(window.localStorage.getItem(k));
};

const storageRemove = function (k) {
    window.localStorage.removeItem(k);
};

document.addEventListener('deviceready', () => {

    const app = new Vue({
        el: '#mainView',
        data: {
            turpial: {
                serviceUUID: "6e400001-b5a3-f393-e0a9-e50e24dcca9e",
                txCharacteristic: "6e400002-b5a3-f393-e0a9-e50e24dcca9e",
                rxCharacteristic: "6e400003-b5a3-f393-e0a9-e50e24dcca9e"
            },
            autoMsg: { active: false, period: 5, timer: null },
            devicesList: [],
            paired: {},
            writeWithoutResponse: null,
            contacts: [],
            chats: [],
            actualChat: {},
            myID: null,
            wallet: {
                initiated: false,
                txs_preview: [{ type: "in", txid: "ccd22fb268d1504b76a655c345dca65b1c83dd2d234b227c996af10adaeedb79" }],
                txin: [{ txid: "ccd22fb268d1504b76a655c345dca65b1c83dd2d234b227c996af10adaeedb79", time: 1554817583, amount: 0.001392, fee: 0.0008 }],
                txout: []
            }
        },
        methods: {
            scanDevices: function (e) {
                console.log('scanning....');
                this.devicesList = [];
                ble.scan([], 10, this.onDiscoverDevice, this.onError);
            },
            connect: function (dev) {
                console.log('connecting....');
                ble.connect(dev.id, this.onConnect, this.onError);
            },
            disconnect: function (dev) {
                console.log('disconnecting....');
                ble.disconnect(dev.id, this.onDisconnect, this.onError);
            },
            onDiscoverDevice: function (dev) {
                console.log('discovered....', dev.id);
                this.devicesList.push({
                    id: dev.id,
                    name: dev.name || "Unknow"
                })
            },
            onConnect: function (peripheral) {
                console.log("fired onConnect: ");
                this.devicesList = this.devicesList.filter(function (element) {
                    if (peripheral.id !== element.id) {
                        return element;
                    }
                })
                this.paired = {
                    id: peripheral.id,
                    name: peripheral.name || "Unknow"
                };
                this.determineWriteType(peripheral);
                // subscribe to incoming data.
                ble.startNotification(
                    this.paired.id,
                    this.turpial.serviceUUID,
                    this.turpial.rxCharacteristic,
                    this.onData,
                    this.onError
                );
            },
            onDisconnect: function (dev) {
                this.paired = {};
                this.scanDevices();
            },
            onData_MSG: function (data) {

                var msg = {
                    type: "received",
                    user: data.uid,
                    txt: data.msg,
                    time: new Date().getTime()
                };

                msg.hash160 = this.hash160(msg);

                this.contacts.forEach(function (e) {
                    if (e.id === msg.user) {
                        e.chat.push(msg);
                    }
                });
            },
            onData_ACK: function (data) {
                alert(data.msg);
            },
            onData_ERR: function (d) {
                alert(data.msg);
            },
            onData: function (d) {

                var strData = this.bytesToString(d);
                console.log("Incoming data:", strData);

                /*
                var data = JSON.parse(strData);
                switch (data.type) {
                    case 'MSG':
                        this.onData_MSG(data);
                        break;
                    case 'ACK':
                        this.onData_ACK(data);
                        break;
                    case 'ERR':
                        this.onData_ERR(data);
                    default:
                        console.log('unknow type:', data.type);
                        break;
                };
                */
                this.contacts.forEach(function (contact) {
                    if(contact.id === "broadcast"){
                        contact.chat.push({
                            type: "received",
                            txt: strData,
                            time: new Date().getTime(),
                            hash160: ""
                        });
                    }
                });

            },
            sendData: function (user, txt) {

                this.actualChat = {
                    type: "sent",
                    user: user,
                    txt: txt,
                    time: new Date().getTime()
                };

                this.actualChat.hash160 = this.hash160(JSON.stringify(this.actualChat));

                var toSend = JSON.stringify({
                    uid: user,
                    msg: txt,
                    hash: this.actualChat.hash160,
                    time: this.actualChat.time.toString()
                });
                console.log(toSend);
                // if not ack
                if (this.writeWithoutResponse) {
                    ble.writeWithoutResponse(
                        this.paired.id,
                        this.turpial.serviceUUID,
                        this.turpial.txCharacteristic,
                        this.stringToBytes(toSend),
                        this.success,
                        this.failure
                    );
                } else {
                    ble.write(
                        this.paired.id,
                        this.turpial.serviceUUID,
                        this.turpial.txCharacteristic,
                        this.stringToBytes(toSend),
                        this.success,
                        this.failure
                    );
                }

            },
            success: function (evt) {
                var self = this;

                this.contacts.forEach(function (user) {
                    if (user.id === self.actualChat.user) {
                        //
                        user.chat.push(self.actualChat);
                        user.draft_input = "";
                    }
                })

                this.actualChat = {};

            },
            failure: function (err) {
                if (err === "Peripheral null not found.") {
                    alert("Turpial or Harpia not found. Go to settings and pair your phone.");
                }
            },
            determineWriteType: function (peripheral) {
                var self = this;
                var characteristic = peripheral.characteristics.filter(function (element) {
                    if (element.characteristic.toLowerCase() === self.turpial.txCharacteristic) {
                        return element;
                    }
                })[0];

                if (characteristic.properties.indexOf('WriteWithoutResponse') > -1) {
                    this.writeWithoutResponse = true;
                } else {
                    this.writeWithoutResponse = false;
                }

            },
            onError: function (err) {
                console.log("fired Error: ", JSON.stringify(err));
            },
            stringToBytes: function (string) {
                var array = new Uint8Array(string.length);
                for (var i = 0, l = string.length; i < l; i++) {
                    array[i] = string.charCodeAt(i);
                }
                return array.buffer;
            },
            bytesToString: function (buffer) {
                return String.fromCharCode.apply(null, new Uint8Array(buffer));
            },
            // crypto functions.
            hash160: function (buff) {
                return lochaCrypto.bitcoinjs.crypto.hash160(buff).toString('hex');
            },
            ECPair: function () {
                const keyPair = lochaCrypto.bitcoinjs.ECPair.makeRandom();
                const { address } = lochaCrypto.bitcoinjs.payments.p2pkh({ pubkey: keyPair.publicKey });

                this.myID = {
                    keyPair: keyPair,
                    address: address
                };
            },
            sendAutoMsgs: function (user) {
                var count = 0;

                if (this.autoMsg.active) {

                    this.autoMsg.timer = setInterval(() => {
                        this.sendData(user, count.toString());
                        count++;
                    }, this.autoMsg.period * 1000);
                } else {
                    console.log("auto-messages", this.autoMsg.active);
                    clearInterval(this.autoMsg.timer);
                }

            },
            purgeChat: function (user) {
                console.log("purge chats from: ", user)

                this.contacts.forEach(function (e) {
                    if (e.id === user) {
                        e.chat = [];
                    }
                });
            },
            txinById: function (id) {
                var tx = this.wallet.txs_preview.filter(function (e) {
                    if (e.id === id) {
                        return e;
                    }
                });
            }
        },
        filters: {
            posixToDateTime: function (timestamp) {
                var dt = new Date(timestamp);
                var date = `${dt.toDateString()} ${dt.getHours()}:${dt.getMinutes()}:${dt.getSeconds()}`;
                return date;
            }
        },
        watch: {
            chats: {
                handler: function () {
                    console.log('updated chats Array');
                },
                deep: true
            },
            contacts: {
                handler: function () {
                    storageSet('contacts', JSON.stringify(this.contacts))
                    console.log("contact list changed");
                    this.chats = this.contacts.filter(function (e) {
                        if (e.id === "broadcast" || e.chat.length) {
                            return e;
                        }
                    });
                },
                deep: true
            },
            autoMsg: {
                handler: function () {
                    console.log("auto-msg");

                },
                deep: false
            },
            myID: {
                handler: function () {
                    storageSet('myID', JSON.stringify(this.myID));
                    console.log("Changed ID");
                },
                deep: false
            }
        },
        mounted: function () {
            this.myID = storageGet('myID' || null);
            this.contacts = storageGet('contacts' || null);
            // only in dev stage
            // this.contacts = demoContacts;
            // this.contacts = []

            if (!this.contacts) {
                console.log("** no hay contactos **")
                this.contacts = demoContacts;
            }

            this.scanDevices();
        }
    })
}, false);
