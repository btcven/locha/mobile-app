cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "com.phonegap.plugins.barcodescanner.barcodescanner",
    "file": "plugins/com.phonegap.plugins.barcodescanner/www/barcodescanner.js",
    "pluginId": "com.phonegap.plugins.barcodescanner",
    "clobbers": [
      "plugins.barcodeScanner"
    ]
  },
  {
    "id": "cordova-plugin-ble-central.ble",
    "file": "plugins/cordova-plugin-ble-central/www/ble.js",
    "pluginId": "cordova-plugin-ble-central",
    "clobbers": [
      "ble"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "com.phonegap.plugins.barcodescanner": "0.6.1",
  "cordova-plugin-ble-central": "1.1.9"
};
// BOTTOM OF METADATA
});