# Locha Mesh Mobile Application.
Mobile App for the locha mesh radio network

## Install

### android

- Last debug: [app-debug.apk](https://gitlab.com/btcven/locha/mobile-app/blob/master/app.locha.io/platforms/android/app/build/outputs/apk/debug/app-debug.apk)
- Release: app-release.apk

### iOS

- Last debug: 
- Release: 

## License 

Copyright (c) 2019 Bitcoin Venezuela and Locha Mesh developers.

Licensed under the **Apache License, Version 2.0**

---
**A text quote is shown below**

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Read the full text:
[Locha Mesh Apache License 2.0](LICENSE)